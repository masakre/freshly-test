import React, { useState, useContext, useMemo } from 'react'
import PropTypes from 'prop-types'
import { Alert, Row, Table, Form, DropdownButton, Dropdown, Modal, Col } from 'react-bootstrap'
import { OrdersPageContext } from '../../pages/Orders/OrdersPage';
import './OrdersList.css'

const ORDER_STATES = {
  PAYMENT_ACCEPTED: 2,
  SHIPPED: 4
}


function OrdersList ({ orders, onOrderStateChange }) {

  const [filter, setFilter] = useState({ filterBy: '', value: '' });
  const [selectedOrder, setSelectedOrder] = useState();
  const { countries } = useContext(OrdersPageContext);

  const handleCloseModal = () => setSelectedOrder(null);

  const handleOnFilterChange = (e) => {
    let { name: filterBy, value } = e.target;
    if (!value) filterBy = '';

    setFilter({ filterBy, value });
  }


  const ordersToShow = (orders.length && filter.filterBy) ?
    orders.filter(order => {
      // check if there is some dots
      const dotIdx = filter.filterBy.indexOf('.');
      if (dotIdx === -1) return (order[filter.filterBy] == filter.value)
      else {
        // if there is a dot, build the path and compare filter.value 
        const [key1, key2, ...useless] = filter.filterBy.split('.');
        return (order[key1][key2] == filter.value)
      }
    }) : orders;


  return <>

    <Row className="orders-filter mb-2">
      <Col>
        <Form name="state-filter" className="bg-light p-2 rounded">
          <Form.Group controlId="state-filter">
            <Form.Label>Filter by order id</Form.Label>
            <Form.Control
              name="id"
              as="input"
              size="sm"
              type="text"
              onChange={handleOnFilterChange}
              value={(filter.filterBy === 'id') ? filter.value : ''}
            />
          </Form.Group>
        </Form>
      </Col>

      <Col>
        <Form name="state-filter" className="bg-light p-2 rounded">
          <Form.Group controlId="state-filter">
            <Form.Label>Filter by state</Form.Label>
            <Form.Control
              name="current_state"
              as="select"
              size="sm"
              custom
              onChange={handleOnFilterChange}
              value={(filter.filterBy === 'current_state') ? filter.value : ''}
            >
              <option value="">All</option>
              <option value={ORDER_STATES.PAYMENT_ACCEPTED}>Payment accepted</option>
              <option value={ORDER_STATES.SHIPPED} >Sent</option>

            </Form.Control>
          </Form.Group>
        </Form>
      </Col>
      <Col>
        <Form name="country-filter" className="bg-light p-2 rounded">
          <Form.Group controlId="country-filter">
            <Form.Label>Filter by country</Form.Label>
            <Form.Control
              name="address.id_country"
              as="select"
              size="sm"
              custom
              onChange={handleOnFilterChange}
              value={(filter.filterBy === 'address.id_country') ? filter.value : ''}
            >
              <option value="">All</option>
              {(countries && countries.length) &&
                countries.map(country => <option value={country.id}>{country.name}</option>)
              }

            </Form.Control>
          </Form.Group>
        </Form>
      </Col>
    </Row>

    { (!ordersToShow.length === 0) && <Alert variant="danger">No orders 😑</Alert>}

    {
      (ordersToShow.length > 0) &&
      <>
        <OrdersTable
          orders={ordersToShow}
          onOrderStateChange={onOrderStateChange}
          onOrderClick={setSelectedOrder}
        />

        <Modal id="selected-order-modal" show={!!selectedOrder} onHide={handleCloseModal} size="xl">
          <Modal.Header closeButton>
            <Modal.Title> <small>Order ID</small> {selectedOrder?.id}</Modal.Title>
          </Modal.Header>
          <Modal.Body>

            {(!!selectedOrder) &&
              <OrdersTable
                orders={[selectedOrder]}
                onOrderStateChange={onOrderStateChange}
              />
            }

          </Modal.Body>
        </Modal>
      </>
    }
  </>
}

OrdersList.propTypes = {
  orders: PropTypes.array,
  onOrderStateChange: PropTypes.func,
}


function OrderTableHeader () {
  return <>
    <thead>
      <tr>
        <th>#</th>
        <th>State</th>
        <th>Update Date</th>
        <th>Full name</th>
        <th>Full address</th>
        <th>Country</th>
        <th>Items</th>
      </tr>
    </thead>
  </>;
}

function OrdersTable ({ orders, onOrderStateChange, onOrderClick }) {
  return <>
    <Table className="orders-list" striped bordered hover responsive size="sm">

      <OrderTableHeader />

      <tbody>
        {orders.map((order, index) =>
          <OrderItem
            key={order.id}
            order={order}
            onOrderStateChange={onOrderStateChange}
            onOrderClick={onOrderClick}
          />
        )}
      </tbody>

    </Table>
  </>
}


function OrderItem ({ order, onOrderStateChange, onOrderClick }) {

  const { id, date_upd, current_state, address, associations } = order;
  const { countries, orderStates } = useContext(OrdersPageContext);
  const { id_country } = address;

  const isShipped = (current_state === ORDER_STATES.SHIPPED);

  const [date, time] = date_upd.split(' ');
  const dateArr = date.split('-');
  const friendlyDate = `${dateArr.reverse().join('-')} ${time}`;
  // state memoization to avoid search already found current_state
  const friendlyState = useMemo(() => (orderStates.find(state => state.id == current_state)?.name ?? current_state)
    , [current_state, orderStates]);

  // state memoization to avoid search already found id_country
  const friendlyCountry = useMemo(() => (countries.find(country => country.id == id_country)?.name ?? id_country)
    , [id_country, countries]);

  const friendlyItems = <ul className="orders-list__order-items">
    {
      associations.order_rows.map(item =>
        <li key={item.id}>
          <strong>{item.product_quantity}x</strong> {item.product_name} &nbsp;
          <small className="text-muted">{item.product_reference}</small>
        </li>)
    }
  </ul>;

  return (
    <tr onClick={(e) => {

      // do no open modal when click over a <a>, <button>, .dropdown, .btn
      if (e.target.classList.contains('dropdown')) return;
      if (e.target.classList.contains('btn')) return;
      if (e.target.nodeName === 'A') return;
      if (e.target.nodeName === 'BUTTON') return;

      if (typeof onOrderClick === 'function') onOrderClick(order);
    }}>

      <td>{id}</td>
      <td className="text-center order-state-td">
        <DropdownButton variant="link" className="text-danger" title={friendlyState}>
          <Dropdown.Item disabled={isShipped}
            onClick={() => { onOrderStateChange(order, ORDER_STATES.PAYMENT_ACCEPTED) }}
          >
            Payment accepted
          </Dropdown.Item>

          <Dropdown.Item disabled={isShipped} onClick={() => { onOrderStateChange(order, ORDER_STATES.SHIPPED) }}
          >
            Sent
          </Dropdown.Item>

        </DropdownButton>
      </td>
      <td>{friendlyDate}</td>
      <td>{`${address?.firstname} ${address?.lastname}`}</td>
      <td>{`${address?.address1} ${address?.postcode} ${address?.city}`}</td>
      <td>{friendlyCountry}</td>
      <td>{friendlyItems}</td>
    </tr>
  )
}

OrderItem.propTypes = {
  order: PropTypes.object,
  onOrderStateChange: PropTypes.func,
  onOrderClick: PropTypes.func
}



export default OrdersList

