import React from 'react';
import { NavLink } from "react-router-dom";

import { Nav, Navbar } from "react-bootstrap";


const Header = props => {
  return (
      
      <Navbar bg="light" expand="lg">
        
        <Navbar.Brand href="#home">LGS Freshly Test Case</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link> <NavLink to="/" activeClassName="active" > Home </NavLink></Nav.Link>
          </Nav>
      
        </Navbar.Collapse>
        
      </Navbar>
  );
};

Header.propTypes = {
  
};

export default Header;
