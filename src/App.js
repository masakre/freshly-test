import React from 'react';
import { Route, BrowserRouter as Router } from "react-router-dom";
import { Container } from 'react-bootstrap';
import Header from "./components/Header/Header";
import { OrdersPage } from './pages/Orders/OrdersPage';

function App () {
  return (
    <div className="lgs-freshly-test">
      <Router>

        <Header />

        <Container fluid>
          <Route path="/" component={OrdersPage} />
        </Container>

      </Router>
    </div>
  );
}

export default App;


