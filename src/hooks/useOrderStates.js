import { useEffect, useState } from 'react';
import * as Swal from 'sweetalert2';
const API_URL = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;
const API_OUTPUT_FORMAT = 'JSON';
const API_DISPLAY = 'full';
const TARGET_URL = `${API_URL}/order_states?ws_key=${API_KEY}&output_format=${API_OUTPUT_FORMAT}&display=${API_DISPLAY}`;


const headers = new Headers();
headers.append('Authorization', 'Basic ' + btoa(`${API_KEY}:`));



function useOrderStates (initialState = {}) {

  const [states, setOrderStates] = useState([]);

  useEffect(() => {

    fetch(TARGET_URL)
      .then(response => response.json())
      .then(data => setOrderStates(data.order_states))
      .catch(error => {

        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error?.message || 'Something went wrong',
          footer: '<a href>Why do I have this issue?</a>'
        })
      });
  }, []);

  return states;
}


export default useOrderStates;