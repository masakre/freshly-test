import { useEffect, useState } from 'react';
import * as Swal from 'sweetalert2';

const API_HOST = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;

const getAddressesApiUrl = (ids, output_format = 'JSON', display = 'full') => {
  const filter = (ids && ids.length) ?
    'filter[id]=[' + ids.join('|') + ']' : '';
  return `${API_HOST}/addresses?${filter}&ws_key=${API_KEY}&output_format=${output_format}&display=${display}`
}

const headers = new Headers();
headers.append('Authorization', 'Basic ' + btoa(`${API_KEY}:`));

function useOrders (initialFilter = {}) {

  const [orders, _setOrders] = useState(null);
  const [addressesCache, setAddressesCache] = useState([]);

  useEffect(() => {

    fetchOrders(initialFilter);

    return () => {
      _setOrders([]);
    }
  }, []);


  const fetchOrders = (ordersFilter = {}) => {
    let filter = '';

    // prepare filter param
    if (ordersFilter !== {}) {
      let filterQuery = Object.keys(ordersFilter).map(key => `filter[${key}]=${ordersFilter[key]}`)
      filter = filterQuery.join('&');
    }

    fetch(`${API_HOST}/orders?ws_key=${API_KEY}&output_format=JSON&display=full&${filter}&sort=[date_upd_DESC]&date=1`)
      .then(response => response.json())
      .then(data => setOrders(data.orders))
      .catch(error => {

        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error?.message || 'Something went wrong',
          footer: '<a href>Why do I have this issue?</a>'
        })
      });
  }

  const setOrders = async (newOrders) => {

    if (!newOrders || !newOrders.length) return;

    // extract new addresses to fetch 
    let newIdAddressesToFetch = newOrders
      .map(order => order.id_address_delivery)
      .filter(id => !(addressesCache.find(addr => addr.id == id)));

    // remove duplicates   
    newIdAddressesToFetch = Array.from(new Set(newIdAddressesToFetch))

    let newAddressesCache = [...addressesCache];
    // if there are new addresses to fetch...                        
    if (newIdAddressesToFetch.length) {
      // get them
      const response = await fetch(getAddressesApiUrl(newIdAddressesToFetch));
      const data = await response.json();
      // if addresses cache is not empty add new fetched address
      newAddressesCache = (addressesCache.length) ? [
        ...addressesCache,
        ...data.addresses
      ] : [...data.addresses]

      await setAddressesCache(newAddressesCache);
    }

    // set addresses to orders
    newOrders = newOrders.map(order => ({
      ...order,
      // fill address attr with the new fetched address or already cached address
      address: newAddressesCache.find(addr => addr.id == order.id_address_delivery)
    }))


    _setOrders((orders && orders.length) ?
      [...newOrders, ...orders] : [...newOrders]
    )

  }


  const setOrderState = async (order, state) => {

    const BASE_URL = `${API_HOST}/orders/${order.id}?ws_key=${API_KEY}`;

    try {
      // retrieve the order in xml format
      const response = await fetch(BASE_URL)
      const dataXML = await response.text();

      // parse xml data
      const xmlParser = new DOMParser();
      const xmlDoc = xmlParser.parseFromString(dataXML, 'text/xml');

      // set current_state to xml order
      xmlDoc.getElementsByTagName('order')[0].getElementsByTagName('current_state')[0].childNodes[0].nodeValue = state

      // UPDATE order !!
      const response2 = await fetch(BASE_URL, {
        method: 'POST',
        headers: {
          'Content-type': 'application/xml',
          'Accept': 'application/xml'
        },
        body: (new XMLSerializer()).serializeToString(xmlDoc)
      })

      const data = await response2.text();
      console.log(' set order state end', data);

      if (data) {
        const newOrders = [...orders];
        const orderIdx = newOrders.findIndex(itm => itm.id === order.id);
        newOrders[orderIdx].current_state = state;

        _setOrders(newOrders);
      }

    } catch (error) {

      console.log('%cERROR', 'font-size:15px; font-weight: bold', { error });

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error?.message || 'Something went wrong',
        footer: '<a href>Why do I have this issue?</a>'
      })

    }
  }

  const onNewOrder = (callback) => {
    console.log('onNewOrder', { callback });
  }

  return { orders, setOrderState, onNewOrder, fetchOrders }
}


export default useOrders;