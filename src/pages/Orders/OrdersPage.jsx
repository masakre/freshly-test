import React, { useEffect } from 'react'
import { Alert, Row, Col } from 'react-bootstrap';
import OrdersList from '../../components/OrdersList/OrdersList';
import useOrders from '../../hooks/useOrders';
import useCountries from '../../hooks/useCountries';
import useOrderStates from '../../hooks/useOrderStates';

const getNewerOrderDate = (orders) => {
  return orders.reduce((newer, order) => {
    return newer = (order.date_upd > newer) ?
      order.date_upd : newer;
  }, '0000-00-00 00:00:00')
}

const OrdersPageContext = React.createContext({
  countries: [],
  orderStates: []
})
OrdersPageContext.displayName = 'OrdersPageContext';

let ordersFetchInterval;
const ORDERS_FETCH_TIMEOUT_INTERVAL = 5000;



function OrdersPage (props) {

  const { orders, setOrderState, fetchOrders } = useOrders({
    current_state: '[2|4]'
  });
  const countries = useCountries();
  const orderStates = useOrderStates();

  // when OrdersPage is mounted...
  useEffect(() => {
    if (orders && orders.length) {

      ordersFetchInterval = setInterval(() => {

        const newerOrderDate = getNewerOrderDate(orders)
        fetchOrders({
          current_state: `[2|4]`, // payment_success / sent
          date_upd: `>[${newerOrderDate.replace(' ', '%20')}]`
        })

      }, ORDERS_FETCH_TIMEOUT_INTERVAL)
    }
    // when orders page is unmounted
    return () => {
      clearInterval(ordersFetchInterval)
    }
  })


  return (
    <div id="orders-page">

      <Row>
        <Col xs="12">
          <h1 className="my-4">Orders</h1>
        </Col>
      </Row>

      { (!orders || !orders.length) &&
        <Alert variant="warning"> Orders loading or no orders </Alert>
      }

      { (orders && orders.length && countries.length && orderStates.length) &&
        <OrdersPageContext.Provider
          value={{
            countries,
            orderStates
          }}
        >

          <OrdersList orders={orders} onOrderStateChange={setOrderState} />

        </OrdersPageContext.Provider>
      }
    </div>
  )
}

OrdersPage.propTypes = {

}

export { OrdersPage, OrdersPageContext }


