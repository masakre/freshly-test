# Descripción

Test case implementado por Luis Gonzalez Serra para Freshly Cosmetics.
Herramienta cloud que permita gestionar de forma ágil la
preparación de pedidos.

**Funcionalidades**:
1. Tabla con las siguientes columnas a mostrar:
a. ID order.
b. Fecha del pedido.
c. Nombre y apellidos de la dirección de envío.
d. Dirección de envío.
e. País de envío.
f. Nombre de los productos comprados.
g. Estado del pedido.

2. Filtros que se tienen que poder aplicar:
a. Estado del pedido.
b. País.
c. (opcional) Puedes añadir más filtros.

3. Actualización en tiempo real. Al crear un nuevo pedido este debe aparecer en el
listado.
4. Solo mostrar pedidos en estado “pago aceptado” o “enviado”.
5. Al hacer clic en el listado, abrir un popup con la la información del pedido con los
mismos campos que el punto 1.
6. Poder cambiar el estado del pedido.
7. Utilizar la API de Prestashop para la obtención y modificación de datos.
8. Cualquier otra mejora también será valorada.


# Instalación

1. Clonar este repositorio
2. Mover a la carpeta donde se haya descargado el repositorio y ejecutar ` yarn install`
( en caso de no tener yarn instaldo es probable que `npm install` también funcione)
3. Editar el contenido del fichero .env.development con el endpoint y la key de la API de Prestashop a la que se haya de conectar.
4. Arrancar la aplicación con ` yarn start` (o `npm start`)

Se levantará un servidor web en http://localhost:3000


## Deuda ténica :tw-1f626: :tw-1f629:
- Implementar la recepción de pedidos en tiempo real usando WebSockets. No he encontrado ninguna referencia a ello en la documentación de la API de Prestashop 1.7.

- No he podido comprobar la edición de estado de un pedido debido a un problema de configuración CORS + PUT de mi servidor Apache / Lampp. Me gustaría poder comprobarlo en un entorno debidamente configurado.

## Q&A

** 1. Describe un problema que surgiera en un proyecto en el que estuvieras trabajando y explica cómo lo resolviste. **

Poner en producción un script para monitorear y guardar en una BBDD el estado / datos de una flota de coches eléctricos el cuál no tenía ningún control y enviaba 341324134 peticiones por segundo a la BBDD. Resultando en un cuelgue de la VM donde se hospedaba el proyecto.

Para resolverlo rearracamos la VM varias veces y mirando el consumo de CPU nos dimos cuenta que el proceso 'mysqld' tenía un consumo elevadísimo. Hicimos rollback y comprobamos que era el script en cuestión.

Lo solucioné añadiendo sleeps de 5 o 10 segundos entre peticiones. 



** 2. ¿Qué medidas tomas para estar al día de las novedades en el sector de la programación? **

Más que medidas, considero que es una actitud, querer saber más (y mejor) e intentar ser una persona curiosa.
Igualmente, las "medidas" serían menos Netflix, HBO, etc y más Udemy.com ( o FreeCourseSite.com :tw-1f608:) & Youtube para aprender de developers mejores.

** 3. ¿Qué retos digitales crees que puede tener Freshly Cosmetics en un horizonte de 3 años? **

Desde el desconocimiento de las "entrañas" de Freshly...

- Gestión de muchos y más grandes equipos de developers.
- Gestión de volúmenes muy grandes de pedidos, incidéncias ( > X / segundo)
- Performance del sistema de gestión de pedidos ( posible descentralización? )
- Alta latencia del e-commerce en países lejanos, tanto a nivel de DB, como de CDN.
- Lidiar con diferentes legislaciones.
- Soportar diferentes divisas.

** 4. ¿Cuál crees que sería tu rol en el equipo encargado de afrontar estos retos? ** 

- Entregar apps intuitivas y con un buen performance, también deberían ser fáciles de mantener por futuros developers.
- Ser un Team Player para resolver retos futuros junto al equipo.

** 5.  ¿Cuál consideras que es la principal responsabilidad del ​ Developer ​ en el Freshly Team? **

Como empresa #PeopleFirst entiendo que debería ser ser un buen profesional; ayudar a mis compañeros, afrontar nuevos retos, implicarme en el desarollo de los productos y cumplir con los sprints.
